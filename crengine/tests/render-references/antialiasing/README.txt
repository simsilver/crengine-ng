Render references for antialiasing feature.

width: 640
height: 360
bpp: 32
Font: FreeSerif, 80 px
Background color: white
TextColor: black
Hinting: None
Text shaping: Simple
Kerning: off
Antialiasing: <various>
