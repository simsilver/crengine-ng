#ifndef RC_CONFIG_H
#define RC_CONFIG_H

#include "crengine-ng-config.h"

#cmakedefine PRODUCTVERSION_V @PRODUCTVERSION_V@
#cmakedefine FILEVERSION_V @FILEVERSION_V@

#endif  // RC_CONFIG_H
