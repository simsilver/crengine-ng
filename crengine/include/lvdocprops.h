/***************************************************************************
 *   crengine-ng                                                           *
 *   Copyright (C) 2008,2009,2011 Vadim Lopatin <coolreader.org@gmail.com> *
 *   Copyright (C) 2012 Daniel Savard <daniels@xsoli.com>                  *
 *   Copyright (C) 2017 poire-z <poire-z@users.noreply.github.com>         *
 *   Copyright (C) 2022 Aleksey Chernov <valexlin@gmail.com>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA 02110-1301, USA.                                                   *
 ***************************************************************************/

#ifndef LVDOCPROPS_H
#define LVDOCPROPS_H

// document property names
#define DOC_PROP_AUTHORS        "doc.authors"
#define DOC_PROP_TITLE          "doc.title"
#define DOC_PROP_LANGUAGE       "doc.language"
#define DOC_PROP_DESCRIPTION    "doc.description"
#define DOC_PROP_KEYWORDS       "doc.keywords"
#define DOC_PROP_SERIES_NAME    "doc.series.name"
#define DOC_PROP_SERIES_NUMBER  "doc.series.number"
#define DOC_PROP_ARC_NAME       "doc.archive.name"
#define DOC_PROP_ARC_PATH       "doc.archive.path"
#define DOC_PROP_ARC_SIZE       "doc.archive.size"
#define DOC_PROP_ARC_FILE_COUNT "doc.archive.file.count"
#define DOC_PROP_FILE_NAME      "doc.file.name"
#define DOC_PROP_FILE_PATH      "doc.file.path"
#define DOC_PROP_FILE_SIZE      "doc.file.size"
#define DOC_PROP_FILE_FORMAT    "doc.file.format"
#define DOC_PROP_FILE_FORMAT_ID "doc.file.format.id"
#define DOC_PROP_FILE_CRC32     "doc.file.crc32"
#define DOC_PROP_FILE_HASH      "doc.file.hash"
#define DOC_PROP_CODE_BASE      "doc.file.code.base"
#define DOC_PROP_COVER_FILE     "doc.cover.file"

#endif // LVDOCPROPS_H
